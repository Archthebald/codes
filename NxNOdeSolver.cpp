#include <iostream>
#include <cmath>
#include <tgmath.h>

class NxNOdeSolver {

private:
    //Globals used for dynamic sizing
    double * t {};
    double ** tsoln {};

public:
    NxNOdeSolver() = default;

    double G[5] = {(1),(1.5),(1.833333),(2.083333333),(2.283333333)};
    double alpha[5] = {-0.185, -0.11111111111111, -0.0823, -0.0415, 0};

    double invGa(int order){
        return ( 1 / (G[order] * 1) * alpha[order]);
    }

//    TODO: To avoid the type conversion issues encountered with using the math library I found
//    I have made functions for the few matrix operations that we have.
//    If you find better solutions, by all means help a brother out.
    double** muiltiplyScalar(double** matrix, double scalar, int len){
        double** out = matrix;

        for(int i = 0; i < len; i++){
            for(int j = 0; j < len; j++){
                out[i][j] = out[i][j] * scalar;
            }
        }

        return out;

    }

    double* multiplyVectorScalar(double* vector, double scalar, int len){
        double* out = vector;
        for(int i =0; i < len; i++){
            out[i] = vector[i] * scalar;
        }
        return out;
    }

    double** multiplyVector(double* vector, double** matrix, int len){
        double** out = matrix;

        for(int i = 0; i < len; i++){
            for(int j = 0; j < len; j++){
                double intermediarySum = 0;
                for(int n = 0; n < len; n++){
                    intermediarySum += matrix[i][n] * vector[i];
                }
                out[i][j] = intermediarySum;
            }
        }
        return out;
    }

    double* multiplyVector(double** matrix, double* vector, int len){
        double* out = vector;

        for(int i = 0; i < len; i++){
            double intermediary = 0;

            for(int j = 0; j<len; j++){
                intermediary += vector[j] * matrix[i][j];
            }
            out[i] = intermediary;
        }
        return out;
    }

    double** leftDivide(double** matrix, double scalar, int len){
        double** out = matrix;

        for(int i = 0; i < len; i++){
            for(int j = 0; j < len; j++){
                out[i][j] = out[i][j] / scalar;
            }
        }

        return out;

    }

    double* addVectors(double* vec1, double* vec2, int len){
        double* retVec = new double[len];
        for(int i = 0; i < len; i++){
            retVec[i] = vec1[i] + vec2[i];
        }
        return retVec;
    }

    double* subtractVectors(double* vec1, double* vec2, int len){
        double* retVec = new double[len];
        for(int i = 0; i < len; i++){
            retVec[i] = vec1[i] - vec2[i];
        }
        return retVec;
    }

    double* addScalar(double* vec, double scalar, int len){
        double* retVec = new double[len];
        for(int i = 0; i < len; i++){
            retVec[i] = vec[i] + scalar;
        }
        return retVec;
    }

    double** addScalar(double** mat, double scalar, int len){
        double** retVec = mat;
        for(int i = 0; i < len; i++){
            for(int j = 0; j < len; j++) {
                retVec[i][j] = mat[i][j] + scalar;
            }
        }
        return retVec;
    }

    void solve(int len, double tstart, double tend, double *y, double **A, double J) {

        double h = 0.02;
        int maxk = 1;
        bool tooslow = false;
        bool nofailed = true;
//        Size array initially as ((tend-tstart) / h) * 1.2 truncated
        t = new double[(int)(((tend-tstart)/h) * 1.2)];
        tsoln = new double*[(int)(((tend-tstart)/h) * 1.2)];

        double t = tstart;
        tsoln[0][0] = t;

//      Yp seems to always be a square matrix the same size as the input matrix
        double* yp = multiplyVector(A, y, len);

//        Jacobian section
//        Done in matlab, passed as argument J
//        End Jacobian section

        int k = 1;
        int K = 1;

//        Initialize diff to 0
        double dif[len][maxk+2];
        for(int i = 0; i < len; i++){
            for(int j = 0; j < maxk+2; j++){
                dif[i][j] = 0;
            }
        }

        for(int i = 0; i < len; i++){
            dif[0][i] = yp[i] * h;
        }

        double hinvGak = h * invGa(k);

        double neq = len;

        int maxit = 4;

        int i = 2;
        bool done = false;

//        Beginning of main loop
        while(!done){
            boolean gotynew = false;

//            ynew loop
            while(!gotynew){

                /*Why*/h = 0.2;
                double* psi = new double[len];

//                this is fun
                psi = multiplyVectorScalar(dif[k], (G[k] * invGa(k)), len);

                double tnew = t + h;

                double difsum = 0;
                for(int i =0; i < len; i++){
                    difsum += dif[k-1][i];
                }
                double* pred = y;

                for(int i =0; i < len; i++){
                    pred[i] = pred[i] + difsum;
                }
//                line 58

                double* ynew = pred;

                double* difkp1 = new double[neq];

                for(int iter = 0; i <= maxit; i++){
//                    double* rhs = hinvGak * A * ynew;
//                    What have I done
                    double* rhs = subtractVectors(multiplyVectorScalar(multiplyVector(A, ynew, len), hinvGak, len), addVectors(psi, difkp1, len), len);

                    double** eye = new double*[len];

//                    Create an identity matrix of size len x len
                    for(int i = 0; i < len; i++){
                        eye[i] = new double[len];
                    }
                    for(int i = 0;i < len; i++){
                        for(int j = 0;j < len; j++){
                            if(i != j) eye[i][j] = 0;
                            else eye[i][i] = 1;
                        }
                    }

                    double** lhs = addScalar(A, (-(hinvGak * J)), len);

                    double* del = NULL;
//                    TODO: Left divide provides solution to lhs * del = rhs
//                    so del = rhs * lhs inv.  Calculate lhs inverse

                    difkp1 = addVectors(difkp1, del, len);

                    ynew = addVectors(pred, difkp1, len);

                    double norm = 0;
                    for(int i = 0; i < len; i++){
                        norm += del[i] * del[i];
                    }
                    norm = sqrt(norm);

                    if(norm <= 10^(-15)){
                        break;
                    }
                    else if(iter == maxit){
                        tooslow = true;
                        break;
                    }
                }
                if(tooslow){
                    h = h/2;
                }
                gotynew = true;
            }
            nofailed = false;

        }

    }


    double** getSoln(){
        return NxNOdeSolver::tsoln;
    }

    double* getTSoln(){
        return NxNOdeSolver::t;
    }
};