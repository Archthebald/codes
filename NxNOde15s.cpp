/*==========================================================
 * NxNOde15s.c
 *
 * The calling syntax is:
 *
 *		[tOutVector, yOutMatrix] = NxNOde15s(aRowSize, tstart, tend, y, A, Jacobian);
 *
 * This is a MEX-file for MATLAB.
 * Copyright 2007-2012 The MathWorks, Inc.
 *
 *========================================================*/
// The code has been modified by Kuhn and Russell to pass an NxN input 
// (W). In it passed in as an NxN matrix from Matlab. Then read in as
// a one dimensional array. In the mex function, we convert it back to 
// an NxN matrix wDoubleArray.

#include "mex.h"
#include "NxNOdeSolver.cpp"


/* The gateway function */
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    double tstart;                  /* tstart scalar */
    double tend;                    /* tend scalar */
    double jacobian;                /* jacobian scalar */
    double * yMatrix;               /* 1xN input vector */
    size_t mRows;                   /* size of vector */
    double *WMatrix;                /* input Matrix */

    double *yOutMatrix;             /* output matrix */
    double *tOutMatrix;             /* output matrix */

    /* check for proper number of arguments */
    if(nrhs!=5) {
        mexErrMsgIdAndTxt("MyToolbox:NxNOde15s:nrhs","Five inputs required.");
    }

    if(nlhs!=2) {
        mexErrMsgIdAndTxt("MyToolbox:NxNOde15s:nlhs","Two outputs required.");
    }
    
	/* make sure the first input argument is scalar */
    if( !mxIsDouble(prhs[0]) || 
         mxIsComplex(prhs[0]) ||
         mxGetNumberOfElements(prhs[0])!=1 ) {
        mexErrMsgIdAndTxt("MyToolbox:NxNOde15s:notScalar","Input tstart must be a scalar.");
    }
    
	/* make sure the second input argument is scalar */
	if( !mxIsDouble(prhs[1]) || 
         mxIsComplex(prhs[1]) ||
         mxGetNumberOfElements(prhs[1])!=1 ) {
        mexErrMsgIdAndTxt("MyToolbox:NxNOde15s:notScalar","Input tend must be a scalar.");
    }
    
    /* make sure the initial value vector is double */
    if( !mxIsDouble(prhs[2]) || 
         mxIsComplex(prhs[2])) {
        mexErrMsgIdAndTxt("MyToolbox:NxNOde15s:notDouble","Input vector must be type double.");
    }
    
    /* check that number of columns in second input argument is 1 */
    if(mxGetN(prhs[2])!=1) {
        mexErrMsgIdAndTxt("MyToolbox:NxNOde15s:notColumnVector","Input must be a column vector.");
    }
    

    /* get the value of the scalar inputs  */
    tstart = mxGetScalar(prhs[0]);
    tend = mxGetScalar(prhs[1]);
    jacobian = mxGetScalar(prhs[4]);

    /* create a pointer to the real data in the input matrix  */
    yMatrix = mxGetPr(prhs[2]);
	
    /* get dimensions of the input matrix */
    mRows = mxGetM(prhs[2]);
	
    /* Get all information about input matrix */
    WMatrix  = (double* ) mxGetPr(prhs[3]); 
    int wNumCols = mxGetN(prhs[3]);
    int wNumRows = mxGetM(prhs[3]);
    double **wDoubleArray;
    wDoubleArray = new double * [wNumRows];

    if(mRows != wNumRows){
        mexErrMsgIdAndTxt("MyToolbox:NxNOde15s:incompatibleTypes", "Input vector-matrix pair is not compatible");
    }

    /* convert from single to double pointer matrix */
    int i = 0; int j = 0; int k = 0; 
    for (i=0; i<wNumRows; i++) {
        for  ( j=0; j<wNumCols; j++ ) {
            wDoubleArray[i][j] = WMatrix[k];
            k++;
		}
    }
    
    /* call the computational routine */
    NxNOdeSolver *solver = new NxNOdeSolver;
    solver->solve(wNumRows, tstart, tend, yMatrix, wDoubleArray, jacobian);
    
    /*
	 * We wont know what the size the output array should be until after the
	 * computation, instantiate them then
	 */
    int row_size /* dummy data */ = 2;
    int column_size /* dummy data */ = 2;

	/* create the output matrix */
    plhs[0] /* T vector */ = mxCreateDoubleMatrix(row_size, 1, mxREAL);
    plhs[1] /* Solution Matrix */ = mxCreateDoubleMatrix(row_size, row_size, mxREAL);

    /* get a pointer to the real data in the output matrix */
    tOutMatrix = mxGetPr(plhs[0]);
    yOutMatrix = mxGetPr(plhs[1]);

    //TODO:This is dummy data to show that the conversion is working
    //Convert double to single pointer matrix
	for(int i = 0; i < column_size; i++){
		for(int j = 0; j < row_size; j++){
			yOutMatrix[(i*row_size)+j] = wDoubleArray[i][j];
		}
	}

	for(int i = 0; i < row_size; i++){
	    tOutMatrix[i] = yMatrix[i];
	}

}
