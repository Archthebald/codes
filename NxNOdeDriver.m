                                                                                                                                               clear all

% Load the test data and compile our function
load variables
mex NxNOde15s.cpp

% BREAKDOWN of function call to NxN in matlab 
%[t, y] = OdeNxN('problem', tspan, x0(:),A);
% t = vector
% y = matrix

% Problem = not used in the NxN matlab TODO: what?
% tspan = tstart & tend = scalar real
% x0 = vector of starting y values real
% A = Matrix of reals
jacobian = 1;

[t, y] = NxNOde15s(tspan(1), tspan(2) , x0(:), A, jacobian);